﻿// Title        : Hacker Rank Tests solution main method Class
// Purpose      : Class that consists the main method to run the Hacker Rank tests
// Limitation   : Everytime the main method will be altered for every test done

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20200529     SHPRLK              Tested Rotated array.
// 20200529     SHPRLK              Tested Rotated array.
// 20200518     SHPRLK              Tested How many numbers less.
// 20200527     SHPRLK              Tested Great number of candies solution.
// 20200517     SHPRLK              Tested Climbing Leader Board unoptimized solution.
// 20200428     SHPRLK              Tested Drawing Book.
// 20200424     SHPRLK              Tested Valley Count.
// 20200424     SHPRLK              Created the file.
// ---------------------------------------------------------------------------------------------------------------------


using HackerRankTests.DynamicProgramming.Medium;
using HackerRankTests.Easy;
using HackerRankTests.Medium;
using System;
using System.Collections.Generic;

namespace HackerRankTests
{
    class Program
    {
        static void Main(string[] args)
        {
            //RotateArray ra = new RotateArray();
            //HowManyNumbersLess mnnl = new HowManyNumbersLess();
            //TopKFrequentWords tkfw = new TopKFrequentWords();                 // yet to complete
            //GreatNumberOfCandies gnoc = new GreatNumberOfCandies();
            TwoSum ts = new TwoSum();
            //ClmbingLeaderboard cb = new ClmbingLeaderboard();
            //CoinChangeProblem ccp = new CoinChangeProblem();
            //DrawingBook db = new DrawingBook();
            //    ValleyCount vc = new ValleyCount("UDDDUDUUUDDDUDUUUDDDUDUU");
        }
    }
}
