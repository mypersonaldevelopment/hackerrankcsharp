﻿// Title        : Leetcode How many numbers less
// Purpose      : Class that consists logic to find for any given number in the array, how many numbers hold lesser value
// Limitation   : If unintented inputs are provided, no error handinglin mechanisms are in place


// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20200528     SHPRLK              Created the file and implemented how many numbers less.
// ---------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace HackerRankTests.Easy
{
    class HowManyNumbersLess
    {
        public HowManyNumbersLess()
        {
            int[] values_ = new int[] { 8, 1, 2, 2, 3 };
            int lengthVal = values_.Length;
            int[] result_ = new int[lengthVal];
            result_ = findHowManyLess(values_);
            foreach (int res_ in result_)
            {
                Console.WriteLine(res_);
            }                
        }

        public int[] findHowManyLess(int[] nums)
        {

            int lengthVal_ = nums.Length;
            
            int[] sortedValues_ = new int[lengthVal_];
            Array.Copy(nums, sortedValues_, lengthVal_);                        //Copying array to have it sorted
            Array.Sort(sortedValues_); 
            
            int[] result_ = new int[lengthVal_];                                // result array
                   
            for (int i = 0; i < lengthVal_; i++)                                // Comparing it with the unique ordered list to find the lesser numbers
            {
                result_[i] = Array.IndexOf(sortedValues_, nums[i]);
            }
            return result_;
        }
    }
}
