﻿// Title        : Hacker Rank Drawing Book
// Purpose      : Class that consists logic to count the least number of times the pages to
//                hss to be flipped to get a given page
// Limitation   : If unintended inputs are fed there is no error handling mechanisms implemented


// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20200424     SHPRLK              Implemented Drawing book count logic with constructor chaining to have defaults passed.
// 20200424     SHPRLK              Created the file.
// ---------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace HackerRankTests.Easy
{
    class DrawingBook
    {
        public DrawingBook(int totalPages, int pageToTraverse)
        {
            int count_ = PageCount(totalPages, pageToTraverse);
            Console.WriteLine("Draw " + count_ + " times  " );
        }

        public DrawingBook() : this(6, 2)       // Constructor chaining to avoid nothing being executed
        {
            Console.WriteLine("Chain constructed passed default values of : 6,4");
        }

        public int PageCount(int totalPages, int pageToTraverse)
        {
            if (totalPages % 2 == 0)
            {
                totalPages += 1;                            //Making it odd number to avoid decimal point of 0.5
            }

            if (pageToTraverse % 2 == 0)
            {
                pageToTraverse += 1; totalPages += 1;       //Making it odd number to avoid decimal point of 0.5
            }

            if (((totalPages - pageToTraverse) / 2) > ((pageToTraverse - 1) / 2))
            {
                return ((pageToTraverse - 1) / 2);
            }
            else
            {
                return ((totalPages - pageToTraverse) / 2);
            }

        }
    }
}
