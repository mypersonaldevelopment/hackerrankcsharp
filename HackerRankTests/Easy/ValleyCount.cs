﻿// Title        : Hacker Rank Valley Count
// Purpose      : Class that consists logic to count the number of valleys passed based on String input
// Limitation   : If unintended inputs are fed there is no error handling mechanisms implemented


// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20200424     SHPRLK              Implemented Valley Count logic in the constructor with chained constructor as failsafe.
// 20200424     SHPRLK              Created the file.
// ---------------------------------------------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Text;

namespace HackerRankTests.Easy
{
    class ValleyCount
    {
        public ValleyCount(String path_)
        {
            int elevation_ = 0, totalValleys_ = 0;
            foreach (char c in path_)
            {                
                if (c == 'U')
                {
                    if (elevation_ + 1 == 0)
                    {
                        totalValleys_ += 1;
                    }
                    elevation_ += 1;
                }
                else
                {
                    elevation_ -= 1;
                }
            }
            Console.WriteLine("----movement : " + path_+"   current elevation : " + elevation_ + "   Valleys passed : "+ totalValleys_);
        }
        public ValleyCount() : this("UDDDUDUU")     //Constructor chaining if no value is passed in 
        {            
            
        }
    }
}
