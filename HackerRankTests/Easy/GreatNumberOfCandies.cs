﻿// Title        : Leetcode Great Number of Coins Challenge
// Purpose      : Class that consists logic to say given the extra candies can the array of candies surpass the maximum
// Limitation   : If unintented inputs are provided, no error handinglin mechanisms are in place


// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20200528     SHPRLK              Created the file and Implemented Two sum logic with the method call in the constructor.
// ---------------------------------------------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HackerRankTests.Easy
{
    class GreatNumberOfCandies
    {
        public GreatNumberOfCandies()
        {
            IList<bool> result_ = KidsWithCandies(new int[] { 2, 3, 5, 1, 3 }, 3);
            foreach (bool res_ in result_)
            {
                Console.WriteLine(res_ ? "True" : "False");
            }
        }
        public IList<bool> KidsWithCandies(int[] candies, int extraCandies)
        {
            int maxValue = candies.Max();
            List<bool> resultList = new List<bool>();
            foreach (int candy_ in candies)
            {
                if ((candy_ + extraCandies) >= maxValue)
                {
                    resultList.Add(true);
                }
                else
                {
                    resultList.Add(false);
                }
            }
            return resultList;
        }

    }
}
