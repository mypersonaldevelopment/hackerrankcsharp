﻿// Title        : Leetcode Two sum challenge
// Purpose      : Class that consists logic to find indices that produce a given target sum
// Limitation   : 1. If two occurances of the same number would product the target result, this solution fails
//                2. If unintended inputs are fed to the method there is no error handling for graceful termination


// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20200525     SHPRLK              Implemented Two sum logic with the method call in the constructor.
// 20200525     SHPRLK              Created the file.
// ---------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace HackerRankTests.Easy
{
    class TwoSum
    {
        public TwoSum()
        {
            //int[] nums_ = new int[] { 230, 863, 916, 585, 981, 404, 316, 785, 88, 12, 70, 435, 384, 778, 887, 755, 740, 337, 86, 92, 325, 422, 815, 650, 920, 125, 277, 336, 221, 847, 168, 23, 677, 61, 400, 136, 874, 363, 394, 199, 863, 997, 794, 587, 124, 321, 212, 957, 764, 173, 314, 422, 927, 783, 930, 282, 306, 506, 44, 926, 691, 568, 68, 730, 933, 737, 531, 180, 414, 751, 28, 546, 60, 371, 493, 370, 527, 387, 43, 541, 13, 457, 328, 227, 652, 365, 430, 803, 59, 858, 538, 427, 583, 368, 375, 173, 809, 896, 370, 789 };
            int[] nums_ = new int[] { 3,2,4};
            int target = 6;

            Console.WriteLine("-----------------unoptimized first solution-------------");
            int[] result = FindTwoDualLoop(nums_, target);
            PrintResultArray(result);

            Console.WriteLine("----------------- third solution index find and / or loop-------------");
            FindTwoWithIndexAndLoop(nums_, target);
            PrintResultArray(result);

        }

        public int[] FindTwoDualLoop(int[] nums, int target) {          //Wrote the unoptimized version to get results to compare
            int[] result_ = new int[] { 0, 0 };
            for (int i = 0; i < nums.Length; i++)
            {
                for (int j = (i + 1); j < nums.Length; j++)
                {
                    if (nums[i] + nums[j] == target)
                    {
                        result_[0] = i;
                        result_[1] = j;
                    }
                }
            }
            return result_;
        }


        public int[] FindTwoWithIndexAndLoop(int[] nums, int target)
        {
            int[] result_ = new int[] { 0, 0 };            
            foreach( int value in nums)
            {
                if(Array.IndexOf(nums, (target - value)) > -1)
                {
                    result_[1] = Array.IndexOf(nums, (target - value));
                    result_[0] = Array.IndexOf(nums, value);
                    break;
                }
                
            }
            if( result_[0] == result_[1])
            {
                for (int i = (result_[0] +1); i < nums.Length; i++)
                {
                    if (nums[result_[0]] + nums[i] == target)
                    {                        
                        result_[1] = i;
                        break;
                    }

                }
            }
            return result_;
        }

        public void PrintResultArray(int[] result)                  //Required a print method as multiple methods were written
        {
            foreach (int res_ in result)
                Console.WriteLine(res_);
        }
    }
}
