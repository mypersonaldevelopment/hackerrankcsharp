﻿// Title        : Leetcode Array rotation, positive shift only
// Purpose      : Class that consists logic to rotate array elements a givne number of times in the positive direction
// Limitation   : 1. Not tested for positive shift
//                2. If unintented inputs are provided, no error handinglin mechanisms are in place


// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20200528     SHPRLK              Created the file and implemented Array rotation for positive shift.
// ---------------------------------------------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace HackerRankTests.Easy
{
    class RotateArray
    {
        public RotateArray()
        {
            int[] nums = new int[] { 1, 2, 3, 4, 5, 6, 7 };
            int[] resultArray = new int[nums.Length];
            int shiftBy = 2;
            foreach (int res_ in Rotate(nums, shiftBy))
            {
                Console.WriteLine(res_);
            }
            Console.WriteLine("---------------Copy result------------");
            foreach (int res_ in RotatewithCopy(nums, shiftBy))
            {
                Console.WriteLine(res_);
            }
        }

        public int[] Rotate(int[] nums, int k)
        {
            int numsLength = nums.Length;
            int actualShift = (k) % numsLength;                         // If rotation value is greater than Array size, find actual shift

            int[] tempShiftArray = new int[k + 1];                        // Temporary array to hold the elements to be shifted

            for (int i = (numsLength + actualShift - 1); i >= 0; i--)
            {
                if (i >= numsLength)                                  //transferring values that is to be added to the front
                {
                    tempShiftArray[i % numsLength] = nums[i - actualShift];
                }
                else                                       // Shifting values to the right
                {
                    if (i >= actualShift)
                    {
                        nums[i] = nums[i - actualShift];
                    }
                    else                                   // Transfering back the values copied to the temp array
                    {
                        nums[i] = tempShiftArray[i];
                    }
                }
            }
            return nums;
        }

        public int[] RotatewithCopy(int[] nums, int k)
        {
            int numsLength = nums.Length;
            int[] tempArray = new int[numsLength];
            Array.Copy(nums, k - 1, tempArray, 0, numsLength-k);
            Array.Copy(nums, k, nums, 0, numsLength);
            Array.Copy(tempArray, 0, nums, numsLength - k, numsLength);
            return nums;
        }
    }
}
