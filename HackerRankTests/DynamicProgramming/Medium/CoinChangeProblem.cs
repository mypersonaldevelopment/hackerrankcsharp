﻿
// Title        : Hacker Rank Coing change problem
// Purpose      : Class that consists logic to find out the possiblities of providing change with
//                given combination of coins
// Limitation   : If unintended inputs are fed there is no error handling mechanisms implemented


// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20200424     SHPRLK              Created the file.
// ---------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace HackerRankTests.DynamicProgramming.Medium
{
    class CoinChangeProblem
    {
        public CoinChangeProblem()
        {
            GetCombination(4, new List<int> { 2, 1, 3 });
        }

        static void GetCombination(int expectedValue, List<int> listOfCoins)
        {
            listOfCoins.Sort();
            var myStack = new Stack<int>(listOfCoins);
            Console.WriteLine(myStack);
        }
    }
}
